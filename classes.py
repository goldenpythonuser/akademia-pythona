class House():
	def __init__(self, address="unknown"):
		self.address=address
	def __str__(self):
		return "[Class: '{}'\nAddress: '{}'\nId: '{}']".format(self.__class__.__name__, self.address, id(self))

class Room(House):
	def __init__(self, owner="unknown", address="unknown"):
		super(Room, self).__init__(address=address)
		self.owner=owner
		self.items=[]
	def add_item(self, item):
		self.items.append(item)
	def show_items(self):
		print("Items in room {}:".format(id(self)))
		for i in self.items:
			print("\t"+str(i))

class Item():
	def __str__(self):
		return "Item: {}".format(self.__class__)

class Bed(Item):
	pass

class Desk(Item):
	pass

class Printer(Item):
	pass

house=House("898 Thomas Street Rochester, NY 14606")
my_room=Room(address=house.address)
bed=Bed()
my_room.add_item(bed)
my_room.add_item(Desk())

print(my_room)
my_room.show_items()
print(isinstance(bed, Item), isinstance(my_room, object), isinstance(Bed, Printer))
input("Enter to exit")