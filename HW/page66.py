def get_num(text):
	correct=False
	while not correct:
		try:
			x=int(input(text))
			correct=True
		except:
			pass
	return x
	

def greeting():
	name=input("Podaj swoje imię: ")
	if name=="Paweł":
		print("Witaj Pawle!")
	else:
		print("Cześć, {}".format(name))
		
def number():
	x=get_num("Podaj liczbę: ")
	if x:
		print(x)
	else:
		print("Zero")
		
class Cuboid():
	def __init__(self, a, b, c):
		self.a=a
		self.b=b
		self.c=c
	def __str__(self):
		return str(self.__class__)+" ({},{},{})".format(self.a, self.b, self.c)
	@property
	def volume(self):
		return self.a*self.b*self.c
	def __lt__(self, other):
		return self.volume < other.volume
	def __eq__(self, other):
		return self.volume == other.volume
	def __le__(self, other):
		return self.volume <= other.volume
		
class Circle():
	def __init__(self, r):
		self.r=r
	def __str__(self):
		return str(self.__class__)+" (r={})".format(self.r)
	@property
	def area(self):
		return 3.14*self.r**2
	def __lt__(self, other):
		return self.area < other.area
	def __eq__(self, other):
		return self.area == other.area
	def __le__(self, other):
		return self.area <= other.area

def comparisons(a, b):
	print("A: {}\nB: {}".format(a,b))
	print("a==b: {}".format(a==b))
	print("a>b: {}".format(a>b))
	print("a<b: {}".format(a<b))
	print("a>=b: {}".format(a>=b))
	print("a<=b: {}".format(a<=b))
	print("a!=b: {}".format(a!=b))

greeting()
print()

number()
print()

a=Cuboid(2,4,5)
b=Cuboid(3,2,4)
comparisons(a,b)
print()

a=b=Circle(4)
comparisons(a,b)
print()

class Vehicle():
	def __init__(self, weight, wheels=4):
		self.weight=weight
		self.wheels=wheels
	def __lt__(self, other):
		return self.weight < other.weight
	def __eq__(self, other):
		return self.weight == other.weight
	def __le__(self, other):
		return self.weight <= other.weight
		
class Car(Vehicle):
	def __init__(self, weight, variety=4):
		super(Car, self).__init__(weight, 4)
		self.variety=["MICRO", "SEDAN", "CUV", "SUV", "HATCHBACK", "ROADSTER", "COUPE", "CABRIOLET"][variety]
	
class Motorcycle(Vehicle):
	def __init__(self, weight):
		super(Motorcycle, self).__init__(weight, 2)
		
class Truck(Vehicle):
	pass
		
class Sedan(Car):
	def __init__(self, weight):
		super(Sedan, self).__init__(weight, 1)
		
class Ice_Cream_Truck(Truck):
	def __init__(self, weight, wheels, color="white"):
		super(Truck, self).__init__(weight, wheels)
		self.color=color
